var main = function() {
  /* Push the body and the nav over by 285px over */
  $('.icon-galerie').click(function() {
    $('.galerie').animate({
      left: "0px"
    }, 200);

    $('body').animate({
      left: "250px"
    }, 200);
  });

  /* Then push them back */
  $('.icon-close').click(function() {
    $('.galerie').animate({
      left: "-250px"
    }, 200);

    $('body').animate({
      left: "0px"
    }, 200);
  });



$.fn.slideSwitch =  function (options){
  var defaults = {
    effectTime: 1000
  };
  var opts = $.extend(defaults, options);
    $(this).each(function(){      
      var $active = $(this).children('img.active');    
      var $next = $active.next();     
      var $next =  $active.next().length ? $active.next(): $(this).children('img:first');
      $active.addClass('last-active');
      $next.css({opacity: 0.0}).addClass('active').animate({opacity: 1.0}, defaults.effectTime, function(){
        $active.removeClass('active last-active');
      });
    })
};

setInterval("$('#slideshowV01').slideSwitch({ effectTime: 1000 })",3500); 
  





};

$(document).ready(main);